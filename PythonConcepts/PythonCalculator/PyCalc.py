mess1 = "Enter two numbers when prompted to do so"
mess2 = "Then enter an arithmetic command when prompted to do so"
strMessage = "Please enter a number when prompted"
strMessage2 = "Please enter an arithmetic command"
counter = 0


def number1():
    print(strMessage)
    num1 = float(input("Number 1: "))
    if num1 < 0:
        print("Number cannot be less than 0")
        number1()
    elif num1 > 500:
        print("Number cannot be more than 500")
        number1()
    else:
        return num1


def number2():
    print(strMessage)
    num2 = float(input("Number 2: "))
    if num2 < 0:
        print("Number cannot be less than 0")
        number2()
    elif num2 > 500:
        print("Number cannot be more than 500")
        number2()
    else:
        return num2


def math(numbe1, numbe2):
    print(strMessage2)
    cmdmath = input("Enter 'add' to Add numbers, 'sub' to subtract, 'mul' to multiply, 'div' to divide: ")
    if cmdmath == "add":
        tot = numbe1 + numbe2
        return tot
    elif cmdmath == "sub":
        tot = numbe1 - numbe2
        return tot
    elif cmdmath == "mul":
        tot = numbe1 * numbe2
        return tot
    elif cmdmath == "div":
        tot = numbe1 / numbe2
        return tot
    else:
        print("Error RM-INVCMD: Commands must be one of the above....Exiting")
        exit()


print(mess1)
print(mess2)
number1 = number1()
number2 = number2()
print(math(number1, number2))







