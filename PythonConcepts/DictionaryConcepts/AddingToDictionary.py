dictionary1 = {
    "item1": ["Spam", "Eggs", "Holy Grail"],
    "item2": ["Horses", "Killer Rabbit", "Black Knight"],
}
print("Adding items to a dictionary")
print(dictionary1["item1"])
print(dictionary1["item2"])
print("----------------------------------------")
choice = input("Add an item? (Y)es or (N)o: ")
print("----------------------------------------")
if choice == "Y" or choice == "y":
    dictionary1["item3"] = ["These are from Monty Python"]
    print(dictionary1["item1"])
    print(dictionary1["item2"])
    print("----Added Definition----")
    print(dictionary1["item3"])
elif choice == "N" or choice == "n":
    print("Fine...")
    exit()
else:
    print("Either Yes or No")
    exit()
