matrix1 = {
    "ln1": [1, 2, 3],
    "ln2": [4, 5, 6],
    "ln3": [7, 8, 9],
}  # end of matrix 1
matrix2 = {
    "rw1": [9, 8, 7],
    "rw2": [6, 5, 4],
    "rw3": [3, 2, 1],
}  # end of matrix 2


def swapmatrices():
    swappedmatrix = {
        "swap1": [matrix1["ln1"]],
        "swap2": [matrix2["rw3"]],
        "swap3": [matrix1["ln3"]],
        "swap4": [matrix2["rw2"]],
    }
    print("Matrix 1: ")
    print("--------------------")
    print(matrix1["ln1"])
    print(matrix1["ln2"])
    print(matrix1["ln3"])
    print("----END MATRIX 1----")
    print("Matrix 2: ")
    print("--------------------")
    print(matrix2["rw1"])
    print(matrix2["rw2"])
    print(matrix2["rw3"])
    print("----END MATRIX 2----")
    print("Swapped Matrix: ")
    print("--------------------")
    print(swappedmatrix["swap1"])
    print(swappedmatrix["swap2"])
    print(swappedmatrix["swap3"])
    print(swappedmatrix["swap4"])
    print("----END OF SWAP MATRIX----")


print("Matrix Swapping")
choice = input("Enter 'S' to swap matrices: ")
if choice == "s" or choice == "S":
    print("Calling Matrix Function.......")
    swapmatrices()
else:
    print("Fine that's cool too...")
    exit()


