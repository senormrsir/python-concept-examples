grades = {
    "John": 98.0,
    "Tom": 80.0,
    "Janet": 65.0,
    "Chris": 70.0,
    "Tammy": 20.0,
}
othergrades = {
    "john": {78.0, 100.0, 89.0},
    "tom": {99.0, 79.0, 88.5},
    "janet": {96.0, 99.0, 80.0},
    "chris": {81.9, 74.2, 60.3},
    "tammy": {92.0, 95.5, 58.3},
}
fullpoints = 175


def gradedecimal(grade):
    decgrade = grade / fullpoints
    return decgrade


def gradeactual(gradedec):
    actualgrade = gradedec * 100
    return actualgrade


print("Grades for students from points")
print("-------------------------------")
choice = input("Who are you?: ")
print("-------------------------------")
if choice == "john" or choice == "John":
    decimal = gradedecimal(float(grades["John"]))
    gradeact = gradeactual(decimal)
    print("Grade on test was: ", float(grades["John"]), " out of ", float(fullpoints))
    print("Calculated grade is: ", float(gradeact))
    print("All test grades: ", othergrades["john"], " ", float(gradeact))
    print("----End of Report----")
elif choice == "tom" or choice == "Tom":
    decimal = gradedecimal(float(grades["Tom"]))
    gradeact2 = gradeactual(decimal)
    print("Grade on test was: ", float(grades["Tom"]), " out of ", float(fullpoints))
    print("Calculated grade is: ", float(gradeact2))
    print("All test grades: ", othergrades["tom"], " ", float(gradeact2))
    print("----End of Report----")
elif choice == "janet" or choice == "Janet":
    decimal = gradedecimal(float(grades["Janet"]))
    gradeact3 = gradeactual(decimal)
    print("Grade on test was: ", float(grades["Janet"]), " out of ", float(fullpoints))
    print("Calculated grade is: ", float(gradeact3))
    print("All test grades: ", othergrades["janet"], " ", float(gradeact3))
    print("----End of Report----")
elif choice == "chris" or choice == "Chris":
    decimal = gradedecimal(float(grades["Chris"]))
    gradeact4 = gradeactual(decimal)
    print("Grade on test was: ", float(grades["Chris"]), " out of ", float(fullpoints))
    print("Calculated grade is: ", float(gradeact4))
    print("All test grades: ", othergrades["chris"], " ", float(gradeact4))
    print("----End of Report----")
elif choice == "tammy" or choice == "Tammy":
    decimal = gradedecimal(float(grades["Tammy"]))
    gradeact5 = gradeactual(decimal)
    print("Grade on test was: ", float(grades["Tammy"]), " out of ", float(fullpoints))
    print("Calculated grade is: ", float(gradeact5))
    print("All test grades: ", othergrades["tammy"], " ", float(gradeact5))
    print("----End of Report----")
else:
    print("Name not recognized....Exiting")
    exit()


