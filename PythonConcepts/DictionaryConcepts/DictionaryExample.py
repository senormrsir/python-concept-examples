fruits = {"fruit 1": "Apple", "fruit 2": "Mango", "fruit 3": "Banana"}
fruitPrices = {"Apple": "$0.49", "Mango": "$1.50", "Banana": "$0.60"}
carbs = {"carb 1": "Bread", "carb 2": "Waffles", "carb 3": "Pasta"}
carbPrices = {"Bread": "$1.45", "Waffles": "$2.38", "Pasta": "$0.89"}
dairys = {"dairy 1": "Milk", "dairy 2": "Yogurt", "dairy 3": "Creamer"}
dairyPrices = {"Milk": "$3.49", "Yogurt": "$1.49", "Creamer": "$1.00"}
misc = {"misc 1": "Cakes", "misc 2": "Beer", "misc 3": "Vodka"}
miscPrices = {"Cakes": "$4.65", "Beer": "$8.99", "Vodka": "$20.00"}
print("Display a shopping list?: ")
choice = input("Display list?, (Y)es or (N)o: ")
if choice == "y" or choice == "Y":
    print("-----------START LIST----------------")
    print(fruits["fruit 1"] + " " + fruitPrices["Apple"])
    print(fruits["fruit 2"] + " " + fruitPrices["Mango"])
    print(fruits["fruit 3"] + " " + fruitPrices["Banana"])
    print(carbs["carb 1"] + " " + carbPrices["Bread"])
    print(carbs["carb 2"] + " " + carbPrices["Waffles"])
    print(carbs["carb 3"] + " " + carbPrices["Pasta"])
    print(dairys["dairy 1"] + " " + dairyPrices["Milk"])
    print(dairys["dairy 2"] + " " + dairyPrices["Yogurt"])
    print(dairys["dairy 3"] + " " + dairyPrices["Creamer"])
    print(misc["misc 1"] + " " + miscPrices["Cakes"])
    print(misc["misc 2"] + " " + miscPrices["Beer"])
    print(misc["misc 3"] + " " + miscPrices["Vodka"])
    print("---------END LIST--------------------")
elif choice == "n" or choice == "N":
    print("Guess you'll starve....")
    exit()
else:
    print("Learn to read, also you're going to starve")
    exit()
