
def initgate():
    print("Type 'lambda' to play with Lambda examples, Type 'exit' to Exit: ")
    usrIn = input(":: ")
    if usrIn == "lambda":
        print("Okay, let's go!")
        dolambdaEx()
    elif usrIn == "exit":
        print("Exiting...")
        exit()
    else:
        print("Invalid Entry...Recalling")
        initgate()


def dolambdaEx():
    numUno = float(input("Enter a number to square: "))
    print("Lambda function to Square: ")
    print((lambda x: x**2)(numUno))
    print("-----------------------------------")
    numDos = float(input("Enter a number to subtract 5 from: "))
    print("Lambda function to Subtract: ")
    print((lambda x: x-5)(numDos))
    print("-----------------------------------")
    numTres = float(input("Enter a number to add 20 to: "))
    print("Lambda function to add: ")
    print((lambda x: x+20)(numTres))
    print("-----------------------------------")
    numCuatro = float(input("Enter a number to tell if it's Even or Odd: "))
    print("Lambda function to tell if Even or Odd: ")
    eORo = ((lambda x: x%2)(numTres))
    if eORo == 0:
        print("Number is Even")
    else:
        print("Number is Odd")
    dolambdaVar()


def dolambdaVar():
    print("-----------------------------------")
    numCinco = float(input("Enter a number to square: "))
    square = lambda x: x**2
    print("Lambda function using variable 'square': ")
    print(square(numCinco))
    print("-----------------------------------")
    numSeis = float(input("Enter a number to subtract 10 from: "))
    subtract = lambda x: x-10
    print("Lambda function using variable 'subtract': ")
    print(subtract(numSeis))
    print("-----------------------------------")
    numSiete = float(input("Enter a number to add 400 to: "))
    add = lambda x: x+400
    print("Lambda function using variable 'add': ")
    print(add(numSiete))
    print("-----------------------------------")
    numOcho = float(input("Enter a number to tell if it's Even or Odd"))
    mod = lambda x: x%2
    NumCase = (mod(numOcho))
    if NumCase == 0:
        print("Number is Even")
    else:
        print("Number is Odd")


print("~~~~~~~~~Lambda Example~~~~~~~~")
initgate()







